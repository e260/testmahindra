package starter.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


import cucumber.runtime.table.TableConverter;
import cucumber.runtime.xstream.LocalizedXStreams;
import gherkin.pickles.PickleTable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import cucumber.api.DataTable;
import cucumber.api.Transformer;


public class datosdeviaje extends Transformer<DataTable>{
	static XSSFWorkbook workbook;
    static XSSFSheet sheet;
    static XSSFCell cell;	
  
    static int xmax=11;
    static int ymax=1;

    static String datos[][]= new String[ymax][xmax];
	 public static void llenandoDatos(String path) throws IOException{
		  // Import excel sheet.
		  File src=new File(path);   
		  // Load the file.
		  FileInputStream fis = new FileInputStream(src);
		  // Load he workbook.
		  workbook = new XSSFWorkbook(fis);
		  // Load the sheet in which data is stored.
		  sheet= workbook.getSheetAt(0);
		  System.out.println("tamaño: "+sheet.getLastRowNum());
		  for(int i=1; i<=sheet.getLastRowNum(); i++){		  
			 

			  cell = sheet.getRow(i).getCell(0);
			  datos[(i-1)][0]=""+cell.getStringCellValue();	
			  System.out.println(datos[(i-1)][0]);

			  cell = sheet.getRow(i).getCell(1);	  
			  datos[(i-1)][1]=""+cell.getStringCellValue();
			  System.out.println(datos[(i-1)][1]);

			  cell = sheet.getRow(i).getCell(2);	  
			  datos[(i-1)][2]=""+cell.getStringCellValue();
			  System.out.println(datos[(i-1)][2]);

			  cell = sheet.getRow(i).getCell(3);	  
			  datos[(i-1)][3]=""+cell.getStringCellValue();		
			  System.out.println(datos[(i-1)][3]);

			  cell = sheet.getRow(i).getCell(4);	  
			  datos[(i-1)][4]=""+cell.getStringCellValue();
			  System.out.println(datos[(i-1)][4]);

			  cell = sheet.getRow(i).getCell(5);	  
			  datos[(i-1)][5]=""+cell.getStringCellValue();
			  System.out.println(datos[(i-1)][5]);

			  cell = sheet.getRow(i).getCell(6);	  
			  datos[(i-1)][6]=""+cell.getStringCellValue();
			  System.out.println(datos[(i-1)][6]);

			  cell = sheet.getRow(i).getCell(7);	  
			  datos[(i-1)][7]=""+cell.getStringCellValue();
			  System.out.println(datos[(i-1)][7]);

			  cell = sheet.getRow(i).getCell(8);	  
			  datos[(i-1)][8]=""+cell.getStringCellValue();
			  System.out.println(datos[(i-1)][8]);

			  cell = sheet.getRow(i).getCell(9);	  
			  datos[(i-1)][9]=""+cell.getStringCellValue();
			  System.out.println(datos[(i-1)][9]);

			  cell = sheet.getRow(i).getCell(10);	  
			  datos[(i-1)][10]=""+cell.getStringCellValue();
			  System.out.println(datos[(i-1)][10]);
		  }
		  
	  }
	 


	@Override
	public DataTable transform(String path) {
		// TODO Auto-generated method stub
		try {
			llenandoDatos(path);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<List> table = new ArrayList<>();;
		for(int i=0;i<ymax;i++) {
			System.out.println(i);
			List<String> row =  Arrays.asList(datos[i][0],datos[i][1],datos[i][2],datos[i][3],datos[i][4],datos[i][5],datos[i][6],datos[i][7],datos[i][8],datos[i][9],datos[i][10]);
			table.add(row);
		}
		DataTable datatable = DataTable.create(table, Locale.getDefault(), "h1", "h2","h3", "h4","h5", "h6","h7", "h8","h9", "h10","h11");
		//datatable.toTable(table, null);
     	System.out.println(datatable.toString());
		return datatable;
	}

	
}
