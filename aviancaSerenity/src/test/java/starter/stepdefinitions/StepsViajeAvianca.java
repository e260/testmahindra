package starter.stepdefinitions;

import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;
import starter.steps.Viajero;

public class StepsViajeAvianca {
    @Steps(shared = true)
    Viajero viajero;

    @Dado("^que me encuentro en la pagina de avianca colombia$")
    public void que_me_encuentro_en_la_pagina_de_avianca_colombia() {
        viajero.ingresarAvianca();
    }

    @Cuando("^hago la busqueda de un vuelo$")
    public void hago_la_busqueda_de_un_vuelo() {
        viajero.filtrosdelviaje();
    }

    @Cuando("^selecciono una de las cotizaciones que visualizo$")
    public void selecciono_una_de_las_cotizaciones_que_visualizo() {
        viajero.cotizaciones();
    }

    @Entonces("^realizo la compra del vuelo que mejor se me acomoda$")
    public void realizo_la_compra_del_vuelo_que_mejor_se_me_acomoda() {

    }
}
