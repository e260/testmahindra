package starter.pagesobject;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@DefaultUrl("https://www.avianca.com/co/en/")
public class page_ViajeAvianca extends PageObject {

    LocalDate date = LocalDate.now();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    String sFecha, sAnio, sMes, sDia;
    int nMes, nDia;

    @FindBy(xpath = "//button[@value='Accept']")
    WebElement btnAceptarCookies;
    @FindBy(xpath = "//body/div[3]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[1]/section[1]/div[3]/div[2]/ul[1]/li[1]/a[1]/label[1]")
    WebElement rdbIdayVuelta;
    @FindBy(xpath = "/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div[2]/div/div/section/div[3]/div[4]/div[1]/div/form/div/div[2]/div/div/div[1]/fieldset/div/div[1]/div/div[1]/label/div/input[1]")
    WebElement txtOrigen;
    @FindBy(xpath = "//div/form/div/div[2]/div/div/div[1]/fieldset/div/div[1]/div/div[2]/ul/li[1]/div[1]")
    WebElement lstResultadoOrigen;
    @FindBy(xpath = "/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div[2]/div/div/section/div[3]/div[4]/div[1]/div/form/div/div[2]/div/div/div[1]/fieldset/div/div[2]/div[2]/div[1]/label/div/input[1]")
    WebElement txtDestino;
    @FindBy(xpath = "//div/form/div/div[2]/div/div/div[1]/fieldset/div/div[2]/div[2]/div[2]/ul/li[1]/div[1]")
    WebElement lstResultadoDestino;
    @FindBy(xpath = "/html/body/div[3]/div/div[2]/div/div[2]/div/div[2]/div/div[2]/div[1]/div/div[2]/div/div/section/div[3]/div[4]/div[1]/div/form/div/div[2]/div/div/div[2]/fieldset/div/div/div[1]/label/div/div/input")
    WebElement txtFechaSalida;
    @FindBy(xpath = "//div[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[1]/div[2]/fieldset[1]/div[1]/div[1]/div[3]/div[1]/div[1]/div[2]/em[1]")
    WebElement btnMesSiguiente;
    @FindBy(xpath = "//div[1]/div[1]/form[1]/div[1]/div[2]/div[1]/div[1]/div[3]/fieldset[1]/div[1]/div[4]/button[1]")
    WebElement btnBuscarVuelos;
    @FindBy(xpath = "//expander-elem[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/button[1]/i[1]")
    WebElement btnExpandir;
    @FindBy(xpath = "//div[4]/button[2]")
    WebElement btnClaseS;

    public void aceptarcookies(){
        btnAceptarCookies.click();
    }
    public void clicIdayVuelta(){
        rdbIdayVuelta.click();
    }
    public void clicDesde(){
        txtOrigen.click();
        txtOrigen.sendKeys("Cartagena");
        reemplazo(lstResultadoOrigen);
    }
    public void clicHacia(){
        txtDestino.click();
        txtDestino.sendKeys("Bogotá");
        reemplazo(lstResultadoDestino);
    }
    public void fechasdeViaje() {
        txtFechaSalida.click();
        sFecha = date.format(formatter);
        sAnio = year(sFecha);
        sMes = NumberMonth(sFecha);
        sDia = day(sFecha);
        if (sDia.equals("30") || sDia.equals("31")) {
            btnMesSiguiente.click();
            sDia = "1";
        } else {
            nDia = Integer.parseInt(sDia) + 1;
            nMes = Integer.parseInt(sMes) + 1;
        }
        seleccionarFechas(sAnio, sMes, String.valueOf(nDia), "3");
        seleccionarFechas(sAnio, String.valueOf(nMes), String.valueOf(nDia), "4");
    }

    public void clicBuscar(){
        btnBuscarVuelos.click();
    }

    public void cotizaciones(){
        btnExpandir.click();
        btnClaseS.click();
    }

    private void reemplazo(WebElement elemento){
        String s_ciudad = "", s_abreviadoCiudad = "";
        int n_Ciudad = 0;
        s_ciudad = elemento.getText().replace(" ", "").replace("(","").replace(")","").replace(",","");
        n_Ciudad = s_ciudad.length();
        n_Ciudad = n_Ciudad - 3;
        s_abreviadoCiudad = s_ciudad.substring(n_Ciudad);
        WebElement ciudad = getDriver().findElement(By.xpath("//li[@data-city='" + s_abreviadoCiudad + "']"));
        ciudad.click();
    }

    private String day(String fecha){
        return fecha.substring(0, 2);
    }

    private String NameMonth(String fecha){
        String month = fecha.substring(3, 5);
        String mmmm = "Not found";
        switch (month){
            case "01": mmmm = "Enero"; break;
            case "02": mmmm = "Febrero"; break;
            case "03": mmmm = "Marzo"; break;
            case "04": mmmm = "Abril"; break;
            case "05": mmmm = "Mayo"; break;
            case "06": mmmm = "Junio"; break;
            case "07": mmmm = "Julio"; break;
            case "08": mmmm = "Agosto"; break;
            case "09": mmmm = "Septiembre"; break;
            case "10": mmmm = "Octubre"; break;
            case "11": mmmm = "Noviembre"; break;
            case "12": mmmm = "Diciembre"; break;
        }
        return mmmm;
    }

    private String NumberMonth(String fecha){
        String month = fecha.substring(3, 5);
        String mmmm = "Not found";
        switch (month){
            case "01": mmmm = "1"; break;
            case "02": mmmm = "2"; break;
            case "03": mmmm = "3"; break;
            case "04": mmmm = "4"; break;
            case "05": mmmm = "5"; break;
            case "06": mmmm = "6"; break;
            case "07": mmmm = "7"; break;
            case "08": mmmm = "8"; break;
            case "09": mmmm = "9"; break;
            case "10": mmmm = "10"; break;
            case "11": mmmm = "11"; break;
            case "12": mmmm = "12"; break;
        }
        return mmmm;
    }

    private String year(String fecha){
        return fecha.substring(6, 10);
    }

    private void seleccionarFechas(String anio, String mes, String dia, String Div){
        WebElement wDia = getDriver().findElement(By.xpath("//section[1]//div[1]/div[1]/form[1]//div[" + Div + "][@data-month='" + anio + "." + mes + "']/div[3]/*[.='" + dia + "']"));
        Actions accionwDia = new Actions(getDriver());
        accionwDia.moveToElement(wDia);
        accionwDia.click();
        accionwDia.perform();
    }
}
