package starter.steps;

import net.serenitybdd.core.steps.ScenarioActor;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.support.PageFactory;
import starter.pagesobject.page_ViajeAvianca;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class Viajero extends ScenarioActor {

    public Viajero() {
        PageFactory.initElements(getDriver(), this);
    }

    Actor actor = Actor.named("Everth Ramírez");
    @Steps(shared = true)
    page_ViajeAvianca page_ViajeAvianca;

    @Step("#actor ingresa a la pagina de Avianca Colombia")
    public void ingresarAvianca() {
        page_ViajeAvianca.setDefaultBaseUrl("https://www.avianca.com/co/en/");
        page_ViajeAvianca.open();
        page_ViajeAvianca.aceptarcookies();
    }

    @Step("#actor ingresa los datos del vuelo que necesita")
    public void filtrosdelviaje() {
        page_ViajeAvianca.clicIdayVuelta();
        page_ViajeAvianca.clicDesde();
        page_ViajeAvianca.clicHacia();
        page_ViajeAvianca.fechasdeViaje();
        page_ViajeAvianca.clicBuscar();
    }

    @Step("#actor selecciona la cotización de mayor acomodo")
    public void cotizaciones(){
        page_ViajeAvianca.cotizaciones();
    }
}
