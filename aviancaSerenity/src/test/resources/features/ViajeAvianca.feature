# new feature
# Tags: optional
#language: es
Característica: Buscar vuelos en avianca

  Escenario: Se desea cotizar vuelos
    Dado que me encuentro en la pagina de avianca colombia
    Cuando hago la busqueda de un vuelo
    Y selecciono una de las cotizaciones que visualizo
    Entonces realizo la compra del vuelo que mejor se me acomoda
